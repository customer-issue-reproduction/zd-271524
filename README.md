# 271524

https://gitlab.zendesk.com/agent/tickets/271524

## What's going on here?

Testing the functionality of [PlantUML](https://docs.gitlab.com/ee/administration/integration/plantuml.html) on various file types.
